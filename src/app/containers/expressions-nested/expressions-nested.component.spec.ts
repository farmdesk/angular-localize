import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpressionsNestedComponent } from './expressions-nested.component';

describe('ExpressionsNestedComponent', () => {
  let component: ExpressionsNestedComponent;
  let fixture: ComponentFixture<ExpressionsNestedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExpressionsNestedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpressionsNestedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
