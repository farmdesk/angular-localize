import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-plurals',
  templateUrl: './plurals.component.html',
  styleUrls: ['./plurals.component.css']
})
export class PluralsComponent implements OnInit {

  minutes = 0;
  inc(i: number) {
    this.minutes = Math.min(5, Math.max(0, this.minutes + i));
  }

  constructor() { }

  ngOnInit(): void {
  }

}
