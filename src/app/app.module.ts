import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { PluralsComponent } from './containers/plurals/plurals.component';
import { ExpressionsNestedComponent } from './containers/expressions-nested/expressions-nested.component';
import { ExpressionsAlternateComponent } from './containers/expressions-alternate/expressions-alternate.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [BrowserModule, AppRoutingModule, FormsModule],
  declarations: [AppComponent, PluralsComponent, ExpressionsNestedComponent, ExpressionsAlternateComponent, NavbarComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
