import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpressionsAlternateComponent } from './expressions-alternate.component';

describe('ExpressionsAlternateComponent', () => {
  let component: ExpressionsAlternateComponent;
  let fixture: ComponentFixture<ExpressionsAlternateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExpressionsAlternateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpressionsAlternateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
