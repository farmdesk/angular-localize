import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExpressionsAlternateComponent } from './containers/expressions-alternate/expressions-alternate.component';
import { ExpressionsNestedComponent } from './containers/expressions-nested/expressions-nested.component';
import { PluralsComponent } from './containers/plurals/plurals.component';

const routes: Routes = [
  {path: 'plurals', component: PluralsComponent},
  {path: 'expressions-alternate', component: ExpressionsAlternateComponent},
  {path: 'expressions-nested', component: ExpressionsNestedComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }