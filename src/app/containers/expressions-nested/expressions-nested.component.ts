import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-expressions-nested',
  templateUrl: './expressions-nested.component.html',
  styleUrls: ['./expressions-nested.component.css']
})
export class ExpressionsNestedComponent implements OnInit {

  minutes = 2;
  gender = 'female';
  fly = true;
  logo = 'https://angular.io/assets/images/logos/angular/angular.png';
  inc(i: number) {
    this.minutes = Math.min(5, Math.max(0, this.minutes + i));
  }
  male() { this.gender = 'male'; }
  female() { this.gender = 'female'; }
  other() { this.gender = 'other'; }

  constructor() { }

  ngOnInit(): void {
  }

}
