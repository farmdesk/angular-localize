import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-expressions-alternate',
  templateUrl: './expressions-alternate.component.html',
  styleUrls: ['./expressions-alternate.component.css']
})
export class ExpressionsAlternateComponent implements OnInit {

  gender: {id?:string, name?:string} = {};
  genders = [
    // { id: "male", name: $localize`:gender-select-option@@male:male`},
    // { id: "female", name: $localize`:gender-select-option@@female:female`},
    // { id: "other", name: $localize`:gender-select-option@@other:other`}
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
