# [Angular Internationalization](https://angular.io/guide/i18n-overview)

Summerized guide to [common internationalization tasks](https://angular.io/guide/i18n-common-overview)

## Add the localize package

Run `ng add @angular/localize`. This is used to take advantage of Angular's localization features.

## Set the source locale ID

By default, Angular uses `en-US` as the source locale of your project.

Changing the source locale:

1. Open the `angular.json` workspace build configuration file
2. Change the source locale in the `sourceLocale` field, at `projects.{projectName}.i18n.sourceLocale`

Unicode locale ID structure `{language_id}-{locale_extension}`.
Example: `nl-BE`.

## Format data based on locale

[DatePipe](https://angular.io/api/common/DatePipe), [CurrencyPipe](https://angular.io/api/common/CurrencyPipe), [DecimalPipe](https://angular.io/api/common/DecimalPipe), [PercentPipe](https://angular.io/api/common/PercentPipe)

Pipes use the LOCALE_ID token to format data based on rules of each locale.
Overriding current locale example: `{{ amount | currency : 'en-US' }}`

## Prepare component for translation

To mark content for translation, add `i18n` to the tag: 
`<element i18n="{i18n_metadata}">{content_to_translate}</element>`

Metadata structure:
`{meaning}|{description}@@{id}`

* `{meaning}`      -> Meaning or intent of the text within the specific context
* `|{description}` -> Additional information or context
* `@@{id}`         -> Custom identifier

It is also possible to provide any or none of the paramaters.
Identifiers can be reused, though if the content differs, the CLI will give a warning.

More: [i18n-common-prepare](https://angular.io/guide/i18n-common-prepare)

## Work with translation files

To generate the source language file, run:
`ng extract-i18n --output-path src/locale`
This generate a `messages.xlf` file.

To create a translation file for a locale or language:

1. Make a copy of `messages.xlf`,
2. Rename the file `message.{locale}.xlf`

According to Angular docs, these files are to be shipped of to the translators.

In our case, we will have our bitbucket linked to a translation providor,
and receive updates in the form of pull requests. 

## Define locales in the build configuration

1. Open `angular.json`
2. To `"i18n"`, add a `"locales"` object
3. To `"locales"`, add locales and paths as `"{locale}": "src/locale/messages.{locale}.xlf"`

i.e. `"nl": "src/locale/messages.nl.xlf"`

## Generate application variants for each locale

1. Open `angular.json`
2. Add `"localize": true` to `build.options`
3. Build from the command line: `ng build --localize`

Ahead-of-time compilation is required, `"aot": true` (default).

`ng serve` (development) only supports one locale at a time.
To change the locale, in `angular.json`:

1. under `build.configurations` add: `"{locale}": { "localize": [ "{locale}" ] }`
2. under `serve.configurations` add: `"{locale}": { "browserTarget": "{app-name}:build:{locale}" }`

## Running app for development

`ng serve` or `npm start`  
Default language for development is `en`

## Generate updated language file

`npm run extract-i18n`  
This will update `src/locale/messages.xlf` file. 
