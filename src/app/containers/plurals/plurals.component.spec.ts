import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PluralsComponent } from './plurals.component';

describe('PluralsComponent', () => {
  let component: PluralsComponent;
  let fixture: ComponentFixture<PluralsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PluralsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PluralsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
